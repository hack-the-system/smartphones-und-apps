# Smartphones und Apps

Smartphones & Apps verantwortungsvoll im politischen Alltag nutzen

Hier findet ihr alle Vortragsmaterialien zur weiteren Verwendung.
Bitte beachtet, dass sich die Folien gegebenenfalls weiterentwickeln werden.

- [...und was soll ich jetzt nutzen?](https://0xacab.org/hack-the-system/smartphones-und-apps/tree/master/Und%20was%20soll%20ich%20jetzt%20nutzen%3F)
  Tipps zur sicheren Verwendung von Smartphones und Apps
